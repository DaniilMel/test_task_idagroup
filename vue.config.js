module.exports = {
    publicPath: './',
    css: {
      modules: true,
      loaderOptions: {
        sass: {
          prependData: `@import "@/common/variables.scss";`
          
        },
      }
    }
  }