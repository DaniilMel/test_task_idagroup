import Vue from "vue";
import VueRouter from "vue-router";
import Payment from "../views/Payment.vue";
import PaymentInfo from "../views/PaymentInfo.vue";
import PaymentHistory from "../views/PaymentHistory.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: '/', redirect: '/payment' },
    {
      path: "/payment",
      name: "payment",
      component: Payment
    },
    {
      path: "/payment_info",
      name: "paymentInfo",
      component: PaymentInfo,
      params: true,
    },
    {
      path: "/payments_history",
      name: "paymentHistory",
      component: PaymentHistory
    }
  ]
});
