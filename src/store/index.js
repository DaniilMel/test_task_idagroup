import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    paymentHistory: []
  },
  getters: {
    paymentHistory(state) { return state.paymentHistory; }
  },
  actions: {
    addPaymentToHistory(context, payment) {
      context.commit('addPayment', payment);
    }
  },
  mutations: {
    addPayment(state, payment) {
      state.paymentHistory.push(payment);
    }
  },
});
